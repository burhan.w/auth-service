<?php
namespace App\Controllers;
use Firebase\JWT\JWT;
use App\Classes\Authorization;

class ApiController {
    use Authorization;   // include trait
    private $response_data;

    // login with ldap
    public function LoginUser() { 
        $json_response = $this->hasAuthorize('user');
        echo $json_response;
    }

    // login with database
    public function LoginApplication() {
        $json_response = $this->hasAuthorize('application');
        echo $json_response;
    }

}