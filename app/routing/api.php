<?PHP 
    

    $router->map('GET', '/[a:controller]', 'App\Controllers\{controller}Controller@index', 'default-get1');
    $router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get2');
    $router->map('POST', '/[a:controller]', 'App\Controllers\{controller}Controller@index', 'default-pos1');
    $router->map('POST', '/[a:controller]/[a:method]', 'App\Controllers\{controller}Controller@{method}', 'default-post2');
    $router->map('DELETE', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-delete');


    // REST Versioning
    /*
    $router->map('GET', '/v[i:version]/[a:controller]', 'App\Controllers\v{version}\{controller}Controller@index', 'default-get1');
    $router->map('GET', '/v[i:version]/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-get2');
    $router->map('POST', '/v[i:version]/[a:controller]/[a:method]', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-post1');
    $router->map('DELETE', '/v[i:version]/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-delete');
   */
    
    /*
    $router->map('GET', '/login', 'App\Controllers\LoginController@index', 'login');

    $router->map('GET', '/[a:controller]', 'App\Controllers\v{version}\{controller}Controller@index', 'default-get1');
    $router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-get2');
    $router->map('POST', '/[a:controller]/[a:method]', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-post1');
    $router->map('DELETE', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\v{version}\{controller}Controller@{method}', 'default-delete');
    */