<?PHP 
namespace App\Repositories;
use App\Entities\AccountRequest;
// use Doctrine\ORM\Tools\Pagination\Paginator;

class AccountRequestRepository extends BaseRepository{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }

    public function checkRequest($user_id, $service_name, $service_method, $service_format){
        $query = $this->em->createQueryBuilder()
			->select(
                'a.userId',
                'a.serviceName',
                'a.serviceUrl',
                'a.serviceMethod',
                'a.serviceFormat',
                'a.serviceFlag'
			)
            ->from(AccountRequest::class, 'a')    // className::class คือ fully qualified name
            ->where('a.userId=:userId')
            ->andWhere('a.serviceName = :serviceName')
            ->andWhere('a.serviceMethod LIKE :serviceMethod')
            ->andWhere('a.serviceFormat LIKE :serviceFormat')
            ->setParameter('userId', $user_id)
            ->setParameter('serviceName', $service_name)
            ->setParameter('serviceMethod', '%'.$service_method.'%')
            ->setParameter('serviceFormat', '%'.$service_format.'%')
            // ->setParameter('serviceMethod', $service_method)
			->getQuery();   
            
        return $query->getOneOrNullResult();
    }

}