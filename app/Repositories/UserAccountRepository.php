<?PHP 
namespace App\Repositories;
use App\Entities\UserAccount;
// use Doctrine\ORM\Tools\Pagination\Paginator;

class UserAccountRepository extends BaseRepository{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }

    public function list (){
        // return $this->em->getRepository(PersonMaster::class)->findAll();
        $query = $this->em->createQueryBuilder()
			->select(
                'u.userId',
                'u.userPwd'
			)
            ->from(UserAccount::class, 'u')    // className::class คือ fully qualified name
			->getQuery();   
            
        return $query->getResult();
    }

    public function checkUserPwd($user_id, $user_pwd){
        $query = $this->em->createQueryBuilder()
			->select(
                'u.userId',
                'u.userPwd'
			)
            ->from(UserAccount::class, 'u')    // className::class คือ fully qualified name
            ->where('u.userId=:userId')
            ->andWhere('u.userPwd=:userPwd')
            ->setParameter('userId', $user_id)
            ->setParameter('userPwd', $user_pwd)
			->getQuery();   
            
        return $query->getOneOrNullResult();
    }

}