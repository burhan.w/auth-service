<?php 
    namespace App\Repositories;
    use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;
    
    class BaseRepository { 
        private $entity;
        public function __construct() {

            $isDevMode = true;  // switch for develop proxy
            $dbParam = array (
                'url' => getenv("DB_URI")   // get url for connect to database from .env file 
            ); 

            $paths = array(BASE_PATH . "/app/entities");
            $proxiesPath = BASE_PATH."/bootstrap/proxies";
            $config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
            
            // Function ที่สร้างเอง upd: 620607
            $config->addCustomNumericFunction('ROUND', 'App\Classes\DQL\Round');
            // $config->addCustomNumericFunction('TRUNC', 'App\Classes\DQL\Trunc');
            // $config->addCustomNumericFunction('TOCHAR', 'App\Classes\DQL\ToChar');
            //  $config->addCustomNumericFunction('ชื่อฟังก์ชันที่ต้องการสร้าง', 'path ไฟล์')
            $config->addCustomNumericFunction('CONCATHELLO', 'App\Classes\DQL\ConcatHello'); 
            $config->addCustomNumericFunction('FLOOR', 'App\Classes\DQL\Floor');
            $config->addCustomNumericFunction('CEIL', 'App\Classes\DQL\Floor');
            $config->addCustomStringFunction('INITCAP', 'App\Classes\DQL\InitCap');

            // Function ที่ impoort จากของคนอื่นที่มีอยู่แล้ว (beberlei/doctrineextensions)
            $config->addCustomDatetimeFunction('TRUNC', 'DoctrineExtensions\Query\Oracle\Trunc');
            $config->addCustomDatetimeFunction('TOCHAR', 'DoctrineExtensions\Query\Oracle\ToChar');

            // upd: 620607
            $config->setProxyDir($proxiesPath);
            $config->setProxyNamespace('GeneratedProxies');
            
            if($isDevMode){
                $config->setAutoGenerateProxyClasses(true);
            }else{
                $config->setAutoGenerateProxyClasses(false);
            }
            // /upd: 620607

            $this->entity = EntityManager::create($dbParam,$config); 
            $dbh = $this->entity->getConnection(); 

            // $sth = $dbh->prepare("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
            // $sth->execute();
        }

        protected function getEntityManager() {
            return $this->entity;
        }
        
    }
