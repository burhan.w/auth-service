<?PHP 
namespace App\Repositories;
use App\Entities\PersonMaster;
// use Doctrine\ORM\Tools\Pagination\Paginator;

class PersonRepository extends BaseRepository implements BaseInterface{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }

    public function list (){
        // return $this->em->getRepository(PersonMaster::class)->findAll();
        $query = $this->em->createQueryBuilder()
			->select(
                'p.personId',
                'p.fnameTh',
                'p.lnameTh'
			)
            ->from(PersonMaster::class, 'p')    // className::class คือ fully qualified name
			->getQuery();   
            
        return $query->getResult();
    }

    public function get ($id){

    }
    public function save ($data){

    }
    public function delete ($id){

    }

}