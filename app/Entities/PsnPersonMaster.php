<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsnPersonMaster
 *
 * @ORM\Table(name="PSN_PERSON_MASTER")
 * @ORM\Entity
 */
class PsnPersonMaster
{
    /**
     * @var string
     *
     * @ORM\Column(name="PERSON_ID", type="decimal", precision=10, scale=0, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_EXT_ENG_CD", type="decimal", precision=3, scale=0, nullable=true)
     */
    private $prenameExtEngCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_INT_ENG_CD", type="decimal", precision=3, scale=0, nullable=true)
     */
    private $prenameIntEngCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_EXT_TH_CD", type="decimal", precision=3, scale=0, nullable=true)
     */
    private $prenameExtThCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENAME_INT_TH_CD", type="decimal", precision=3, scale=0, nullable=true)
     */
    private $prenameIntThCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_FNAME_TH", type="string", length=30, nullable=true)
     */
    private $personFnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_LNAME_TH", type="string", length=50, nullable=true)
     */
    private $personLnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_FNAME_ENG", type="string", length=30, nullable=true)
     */
    private $personFnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_LNAME_ENG", type="string", length=50, nullable=true)
     */
    private $personLnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SEX_TYPE", type="string", length=1, nullable=true)
     */
    private $sexType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MARRY_STATUS_CD", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $marryStatusCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_TYPE_CD", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $personTypeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_STATUS_CD", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $personStatusCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="REMARK", type="string", length=100, nullable=true)
     */
    private $remark;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_DATE", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_CD", type="decimal", precision=4, scale=0, nullable=true)
     */
    private $deptCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ACTIVE_FLAG", type="string", length=1, nullable=true)
     */
    private $activeFlag;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="CREATION_DTM", type="datetime", nullable=true)
     */
    private $creationDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CREATION_BY", type="string", length=20, nullable=true)
     */
    private $creationBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="LAST_UPDATE_DTM", type="datetime", nullable=true)
     */
    private $lastUpdateDtm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_UPDATE_BY", type="string", length=20, nullable=true)
     */
    private $lastUpdateBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BUASRI_ID", type="string", length=15, nullable=true)
     */
    private $buasriId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="IN_TELEPHONE_NO", type="string", length=50, nullable=true)
     */
    private $inTelephoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LOCATION_CD", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $locationCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BUILD_NO", type="string", length=2, nullable=true)
     */
    private $buildNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ROOM_NO", type="string", length=10, nullable=true)
     */
    private $roomNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BUASRI_PASSWORD", type="string", length=15, nullable=true)
     */
    private $buasriPassword;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_POSITION_TYPE_CD", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $personPositionTypeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ID_CARD_NO", type="string", length=13, nullable=true)
     */
    private $idCardNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_MNAME_TH", type="string", length=50, nullable=true)
     */
    private $personMnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_MNAME_ENG", type="string", length=50, nullable=true)
     */
    private $personMnameEng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PASSPORT_NO", type="string", length=20, nullable=true)
     */
    private $passportNo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE_AT_SWU", type="date", nullable=true)
     */
    private $startDateAtSwu;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="RETIRE_DATE", type="date", nullable=true)
     */
    private $retireDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RETIRE_REASON_CD", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $retireReasonCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RETIRE_DESC", type="string", length=200, nullable=true)
     */
    private $retireDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MOBILE_PHONE", type="string", length=50, nullable=true)
     */
    private $mobilePhone;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="BIRTH_DATE", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="BLOOD_GROUP", type="string", length=2, nullable=true)
     */
    private $bloodGroup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RACE_CD", type="string", length=5, nullable=true)
     */
    private $raceCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NATION_CD", type="string", length=5, nullable=true)
     */
    private $nationCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RELIGION_CD", type="string", length=2, nullable=true)
     */
    private $religionCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DISEASE", type="string", length=200, nullable=true)
     */
    private $disease;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MILITARY_STATUS", type="string", length=1, nullable=true)
     */
    private $militaryStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TOTAL_COUSIN", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $totalCousin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SEQ_COUSIN", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $seqCousin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSONAL_HOMEPAGE", type="string", length=200, nullable=true)
     */
    private $personalHomepage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RETIRE_CAUSE_CD", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $retireCauseCd;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="REPORT_DATE", type="date", nullable=true)
     */
    private $reportDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="WORK_PERIOD_DAY", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $workPeriodDay;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PROGRAM_CD", type="string", length=20, nullable=true)
     */
    private $programCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="RH_BLOOD", type="string", length=3, nullable=true)
     */
    private $rhBlood;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CONTACT_TELEPHONE_NO", type="string", length=50, nullable=true)
     */
    private $contactTelephoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FAX_NO", type="string", length=50, nullable=true)
     */
    private $faxNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DIRECT_PHONE_NO", type="string", length=50, nullable=true)
     */
    private $directPhoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXT_PHONE_NO", type="string", length=50, nullable=true)
     */
    private $extPhoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DID_PHONE_NO", type="string", length=50, nullable=true)
     */
    private $didPhoneNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="EXT_EXT_PHONE_NO", type="string", length=50, nullable=true)
     */
    private $extExtPhoneNo;



    /**
     * Get personId.
     *
     * @return string
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set prenameExtEngCd.
     *
     * @param string|null $prenameExtEngCd
     *
     * @return PsnPersonMaster
     */
    public function setPrenameExtEngCd($prenameExtEngCd = null)
    {
        $this->prenameExtEngCd = $prenameExtEngCd;

        return $this;
    }

    /**
     * Get prenameExtEngCd.
     *
     * @return string|null
     */
    public function getPrenameExtEngCd()
    {
        return $this->prenameExtEngCd;
    }

    /**
     * Set prenameIntEngCd.
     *
     * @param string|null $prenameIntEngCd
     *
     * @return PsnPersonMaster
     */
    public function setPrenameIntEngCd($prenameIntEngCd = null)
    {
        $this->prenameIntEngCd = $prenameIntEngCd;

        return $this;
    }

    /**
     * Get prenameIntEngCd.
     *
     * @return string|null
     */
    public function getPrenameIntEngCd()
    {
        return $this->prenameIntEngCd;
    }

    /**
     * Set prenameExtThCd.
     *
     * @param string|null $prenameExtThCd
     *
     * @return PsnPersonMaster
     */
    public function setPrenameExtThCd($prenameExtThCd = null)
    {
        $this->prenameExtThCd = $prenameExtThCd;

        return $this;
    }

    /**
     * Get prenameExtThCd.
     *
     * @return string|null
     */
    public function getPrenameExtThCd()
    {
        return $this->prenameExtThCd;
    }

    /**
     * Set prenameIntThCd.
     *
     * @param string|null $prenameIntThCd
     *
     * @return PsnPersonMaster
     */
    public function setPrenameIntThCd($prenameIntThCd = null)
    {
        $this->prenameIntThCd = $prenameIntThCd;

        return $this;
    }

    /**
     * Get prenameIntThCd.
     *
     * @return string|null
     */
    public function getPrenameIntThCd()
    {
        return $this->prenameIntThCd;
    }

    /**
     * Set personFnameTh.
     *
     * @param string|null $personFnameTh
     *
     * @return PsnPersonMaster
     */
    public function setPersonFnameTh($personFnameTh = null)
    {
        $this->personFnameTh = $personFnameTh;

        return $this;
    }

    /**
     * Get personFnameTh.
     *
     * @return string|null
     */
    public function getPersonFnameTh()
    {
        return $this->personFnameTh;
    }

    /**
     * Set personLnameTh.
     *
     * @param string|null $personLnameTh
     *
     * @return PsnPersonMaster
     */
    public function setPersonLnameTh($personLnameTh = null)
    {
        $this->personLnameTh = $personLnameTh;

        return $this;
    }

    /**
     * Get personLnameTh.
     *
     * @return string|null
     */
    public function getPersonLnameTh()
    {
        return $this->personLnameTh;
    }

    /**
     * Set personFnameEng.
     *
     * @param string|null $personFnameEng
     *
     * @return PsnPersonMaster
     */
    public function setPersonFnameEng($personFnameEng = null)
    {
        $this->personFnameEng = $personFnameEng;

        return $this;
    }

    /**
     * Get personFnameEng.
     *
     * @return string|null
     */
    public function getPersonFnameEng()
    {
        return $this->personFnameEng;
    }

    /**
     * Set personLnameEng.
     *
     * @param string|null $personLnameEng
     *
     * @return PsnPersonMaster
     */
    public function setPersonLnameEng($personLnameEng = null)
    {
        $this->personLnameEng = $personLnameEng;

        return $this;
    }

    /**
     * Get personLnameEng.
     *
     * @return string|null
     */
    public function getPersonLnameEng()
    {
        return $this->personLnameEng;
    }

    /**
     * Set sexType.
     *
     * @param string|null $sexType
     *
     * @return PsnPersonMaster
     */
    public function setSexType($sexType = null)
    {
        $this->sexType = $sexType;

        return $this;
    }

    /**
     * Get sexType.
     *
     * @return string|null
     */
    public function getSexType()
    {
        return $this->sexType;
    }

    /**
     * Set marryStatusCd.
     *
     * @param string|null $marryStatusCd
     *
     * @return PsnPersonMaster
     */
    public function setMarryStatusCd($marryStatusCd = null)
    {
        $this->marryStatusCd = $marryStatusCd;

        return $this;
    }

    /**
     * Get marryStatusCd.
     *
     * @return string|null
     */
    public function getMarryStatusCd()
    {
        return $this->marryStatusCd;
    }

    /**
     * Set personTypeCd.
     *
     * @param string|null $personTypeCd
     *
     * @return PsnPersonMaster
     */
    public function setPersonTypeCd($personTypeCd = null)
    {
        $this->personTypeCd = $personTypeCd;

        return $this;
    }

    /**
     * Get personTypeCd.
     *
     * @return string|null
     */
    public function getPersonTypeCd()
    {
        return $this->personTypeCd;
    }

    /**
     * Set personStatusCd.
     *
     * @param string|null $personStatusCd
     *
     * @return PsnPersonMaster
     */
    public function setPersonStatusCd($personStatusCd = null)
    {
        $this->personStatusCd = $personStatusCd;

        return $this;
    }

    /**
     * Get personStatusCd.
     *
     * @return string|null
     */
    public function getPersonStatusCd()
    {
        return $this->personStatusCd;
    }

    /**
     * Set remark.
     *
     * @param string|null $remark
     *
     * @return PsnPersonMaster
     */
    public function setRemark($remark = null)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark.
     *
     * @return string|null
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return PsnPersonMaster
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return PsnPersonMaster
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set deptCd.
     *
     * @param string|null $deptCd
     *
     * @return PsnPersonMaster
     */
    public function setDeptCd($deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return string|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set activeFlag.
     *
     * @param string|null $activeFlag
     *
     * @return PsnPersonMaster
     */
    public function setActiveFlag($activeFlag = null)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag.
     *
     * @return string|null
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set creationDtm.
     *
     * @param \DateTime|null $creationDtm
     *
     * @return PsnPersonMaster
     */
    public function setCreationDtm($creationDtm = null)
    {
        $this->creationDtm = $creationDtm;

        return $this;
    }

    /**
     * Get creationDtm.
     *
     * @return \DateTime|null
     */
    public function getCreationDtm()
    {
        return $this->creationDtm;
    }

    /**
     * Set creationBy.
     *
     * @param string|null $creationBy
     *
     * @return PsnPersonMaster
     */
    public function setCreationBy($creationBy = null)
    {
        $this->creationBy = $creationBy;

        return $this;
    }

    /**
     * Get creationBy.
     *
     * @return string|null
     */
    public function getCreationBy()
    {
        return $this->creationBy;
    }

    /**
     * Set lastUpdateDtm.
     *
     * @param \DateTime|null $lastUpdateDtm
     *
     * @return PsnPersonMaster
     */
    public function setLastUpdateDtm($lastUpdateDtm = null)
    {
        $this->lastUpdateDtm = $lastUpdateDtm;

        return $this;
    }

    /**
     * Get lastUpdateDtm.
     *
     * @return \DateTime|null
     */
    public function getLastUpdateDtm()
    {
        return $this->lastUpdateDtm;
    }

    /**
     * Set lastUpdateBy.
     *
     * @param string|null $lastUpdateBy
     *
     * @return PsnPersonMaster
     */
    public function setLastUpdateBy($lastUpdateBy = null)
    {
        $this->lastUpdateBy = $lastUpdateBy;

        return $this;
    }

    /**
     * Get lastUpdateBy.
     *
     * @return string|null
     */
    public function getLastUpdateBy()
    {
        return $this->lastUpdateBy;
    }

    /**
     * Set buasriId.
     *
     * @param string|null $buasriId
     *
     * @return PsnPersonMaster
     */
    public function setBuasriId($buasriId = null)
    {
        $this->buasriId = $buasriId;

        return $this;
    }

    /**
     * Get buasriId.
     *
     * @return string|null
     */
    public function getBuasriId()
    {
        return $this->buasriId;
    }

    /**
     * Set inTelephoneNo.
     *
     * @param string|null $inTelephoneNo
     *
     * @return PsnPersonMaster
     */
    public function setInTelephoneNo($inTelephoneNo = null)
    {
        $this->inTelephoneNo = $inTelephoneNo;

        return $this;
    }

    /**
     * Get inTelephoneNo.
     *
     * @return string|null
     */
    public function getInTelephoneNo()
    {
        return $this->inTelephoneNo;
    }

    /**
     * Set locationCd.
     *
     * @param string|null $locationCd
     *
     * @return PsnPersonMaster
     */
    public function setLocationCd($locationCd = null)
    {
        $this->locationCd = $locationCd;

        return $this;
    }

    /**
     * Get locationCd.
     *
     * @return string|null
     */
    public function getLocationCd()
    {
        return $this->locationCd;
    }

    /**
     * Set buildNo.
     *
     * @param string|null $buildNo
     *
     * @return PsnPersonMaster
     */
    public function setBuildNo($buildNo = null)
    {
        $this->buildNo = $buildNo;

        return $this;
    }

    /**
     * Get buildNo.
     *
     * @return string|null
     */
    public function getBuildNo()
    {
        return $this->buildNo;
    }

    /**
     * Set roomNo.
     *
     * @param string|null $roomNo
     *
     * @return PsnPersonMaster
     */
    public function setRoomNo($roomNo = null)
    {
        $this->roomNo = $roomNo;

        return $this;
    }

    /**
     * Get roomNo.
     *
     * @return string|null
     */
    public function getRoomNo()
    {
        return $this->roomNo;
    }

    /**
     * Set buasriPassword.
     *
     * @param string|null $buasriPassword
     *
     * @return PsnPersonMaster
     */
    public function setBuasriPassword($buasriPassword = null)
    {
        $this->buasriPassword = $buasriPassword;

        return $this;
    }

    /**
     * Get buasriPassword.
     *
     * @return string|null
     */
    public function getBuasriPassword()
    {
        return $this->buasriPassword;
    }

    /**
     * Set personPositionTypeCd.
     *
     * @param string|null $personPositionTypeCd
     *
     * @return PsnPersonMaster
     */
    public function setPersonPositionTypeCd($personPositionTypeCd = null)
    {
        $this->personPositionTypeCd = $personPositionTypeCd;

        return $this;
    }

    /**
     * Get personPositionTypeCd.
     *
     * @return string|null
     */
    public function getPersonPositionTypeCd()
    {
        return $this->personPositionTypeCd;
    }

    /**
     * Set idCardNo.
     *
     * @param string|null $idCardNo
     *
     * @return PsnPersonMaster
     */
    public function setIdCardNo($idCardNo = null)
    {
        $this->idCardNo = $idCardNo;

        return $this;
    }

    /**
     * Get idCardNo.
     *
     * @return string|null
     */
    public function getIdCardNo()
    {
        return $this->idCardNo;
    }

    /**
     * Set personMnameTh.
     *
     * @param string|null $personMnameTh
     *
     * @return PsnPersonMaster
     */
    public function setPersonMnameTh($personMnameTh = null)
    {
        $this->personMnameTh = $personMnameTh;

        return $this;
    }

    /**
     * Get personMnameTh.
     *
     * @return string|null
     */
    public function getPersonMnameTh()
    {
        return $this->personMnameTh;
    }

    /**
     * Set personMnameEng.
     *
     * @param string|null $personMnameEng
     *
     * @return PsnPersonMaster
     */
    public function setPersonMnameEng($personMnameEng = null)
    {
        $this->personMnameEng = $personMnameEng;

        return $this;
    }

    /**
     * Get personMnameEng.
     *
     * @return string|null
     */
    public function getPersonMnameEng()
    {
        return $this->personMnameEng;
    }

    /**
     * Set passportNo.
     *
     * @param string|null $passportNo
     *
     * @return PsnPersonMaster
     */
    public function setPassportNo($passportNo = null)
    {
        $this->passportNo = $passportNo;

        return $this;
    }

    /**
     * Get passportNo.
     *
     * @return string|null
     */
    public function getPassportNo()
    {
        return $this->passportNo;
    }

    /**
     * Set startDateAtSwu.
     *
     * @param \DateTime|null $startDateAtSwu
     *
     * @return PsnPersonMaster
     */
    public function setStartDateAtSwu($startDateAtSwu = null)
    {
        $this->startDateAtSwu = $startDateAtSwu;

        return $this;
    }

    /**
     * Get startDateAtSwu.
     *
     * @return \DateTime|null
     */
    public function getStartDateAtSwu()
    {
        return $this->startDateAtSwu;
    }

    /**
     * Set retireDate.
     *
     * @param \DateTime|null $retireDate
     *
     * @return PsnPersonMaster
     */
    public function setRetireDate($retireDate = null)
    {
        $this->retireDate = $retireDate;

        return $this;
    }

    /**
     * Get retireDate.
     *
     * @return \DateTime|null
     */
    public function getRetireDate()
    {
        return $this->retireDate;
    }

    /**
     * Set retireReasonCd.
     *
     * @param string|null $retireReasonCd
     *
     * @return PsnPersonMaster
     */
    public function setRetireReasonCd($retireReasonCd = null)
    {
        $this->retireReasonCd = $retireReasonCd;

        return $this;
    }

    /**
     * Get retireReasonCd.
     *
     * @return string|null
     */
    public function getRetireReasonCd()
    {
        return $this->retireReasonCd;
    }

    /**
     * Set retireDesc.
     *
     * @param string|null $retireDesc
     *
     * @return PsnPersonMaster
     */
    public function setRetireDesc($retireDesc = null)
    {
        $this->retireDesc = $retireDesc;

        return $this;
    }

    /**
     * Get retireDesc.
     *
     * @return string|null
     */
    public function getRetireDesc()
    {
        return $this->retireDesc;
    }

    /**
     * Set mobilePhone.
     *
     * @param string|null $mobilePhone
     *
     * @return PsnPersonMaster
     */
    public function setMobilePhone($mobilePhone = null)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone.
     *
     * @return string|null
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set birthDate.
     *
     * @param \DateTime|null $birthDate
     *
     * @return PsnPersonMaster
     */
    public function setBirthDate($birthDate = null)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate.
     *
     * @return \DateTime|null
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set bloodGroup.
     *
     * @param string|null $bloodGroup
     *
     * @return PsnPersonMaster
     */
    public function setBloodGroup($bloodGroup = null)
    {
        $this->bloodGroup = $bloodGroup;

        return $this;
    }

    /**
     * Get bloodGroup.
     *
     * @return string|null
     */
    public function getBloodGroup()
    {
        return $this->bloodGroup;
    }

    /**
     * Set raceCd.
     *
     * @param string|null $raceCd
     *
     * @return PsnPersonMaster
     */
    public function setRaceCd($raceCd = null)
    {
        $this->raceCd = $raceCd;

        return $this;
    }

    /**
     * Get raceCd.
     *
     * @return string|null
     */
    public function getRaceCd()
    {
        return $this->raceCd;
    }

    /**
     * Set nationCd.
     *
     * @param string|null $nationCd
     *
     * @return PsnPersonMaster
     */
    public function setNationCd($nationCd = null)
    {
        $this->nationCd = $nationCd;

        return $this;
    }

    /**
     * Get nationCd.
     *
     * @return string|null
     */
    public function getNationCd()
    {
        return $this->nationCd;
    }

    /**
     * Set religionCd.
     *
     * @param string|null $religionCd
     *
     * @return PsnPersonMaster
     */
    public function setReligionCd($religionCd = null)
    {
        $this->religionCd = $religionCd;

        return $this;
    }

    /**
     * Get religionCd.
     *
     * @return string|null
     */
    public function getReligionCd()
    {
        return $this->religionCd;
    }

    /**
     * Set disease.
     *
     * @param string|null $disease
     *
     * @return PsnPersonMaster
     */
    public function setDisease($disease = null)
    {
        $this->disease = $disease;

        return $this;
    }

    /**
     * Get disease.
     *
     * @return string|null
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * Set militaryStatus.
     *
     * @param string|null $militaryStatus
     *
     * @return PsnPersonMaster
     */
    public function setMilitaryStatus($militaryStatus = null)
    {
        $this->militaryStatus = $militaryStatus;

        return $this;
    }

    /**
     * Get militaryStatus.
     *
     * @return string|null
     */
    public function getMilitaryStatus()
    {
        return $this->militaryStatus;
    }

    /**
     * Set totalCousin.
     *
     * @param string|null $totalCousin
     *
     * @return PsnPersonMaster
     */
    public function setTotalCousin($totalCousin = null)
    {
        $this->totalCousin = $totalCousin;

        return $this;
    }

    /**
     * Get totalCousin.
     *
     * @return string|null
     */
    public function getTotalCousin()
    {
        return $this->totalCousin;
    }

    /**
     * Set seqCousin.
     *
     * @param string|null $seqCousin
     *
     * @return PsnPersonMaster
     */
    public function setSeqCousin($seqCousin = null)
    {
        $this->seqCousin = $seqCousin;

        return $this;
    }

    /**
     * Get seqCousin.
     *
     * @return string|null
     */
    public function getSeqCousin()
    {
        return $this->seqCousin;
    }

    /**
     * Set personalHomepage.
     *
     * @param string|null $personalHomepage
     *
     * @return PsnPersonMaster
     */
    public function setPersonalHomepage($personalHomepage = null)
    {
        $this->personalHomepage = $personalHomepage;

        return $this;
    }

    /**
     * Get personalHomepage.
     *
     * @return string|null
     */
    public function getPersonalHomepage()
    {
        return $this->personalHomepage;
    }

    /**
     * Set retireCauseCd.
     *
     * @param string|null $retireCauseCd
     *
     * @return PsnPersonMaster
     */
    public function setRetireCauseCd($retireCauseCd = null)
    {
        $this->retireCauseCd = $retireCauseCd;

        return $this;
    }

    /**
     * Get retireCauseCd.
     *
     * @return string|null
     */
    public function getRetireCauseCd()
    {
        return $this->retireCauseCd;
    }

    /**
     * Set reportDate.
     *
     * @param \DateTime|null $reportDate
     *
     * @return PsnPersonMaster
     */
    public function setReportDate($reportDate = null)
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    /**
     * Get reportDate.
     *
     * @return \DateTime|null
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * Set workPeriodDay.
     *
     * @param string|null $workPeriodDay
     *
     * @return PsnPersonMaster
     */
    public function setWorkPeriodDay($workPeriodDay = null)
    {
        $this->workPeriodDay = $workPeriodDay;

        return $this;
    }

    /**
     * Get workPeriodDay.
     *
     * @return string|null
     */
    public function getWorkPeriodDay()
    {
        return $this->workPeriodDay;
    }

    /**
     * Set programCd.
     *
     * @param string|null $programCd
     *
     * @return PsnPersonMaster
     */
    public function setProgramCd($programCd = null)
    {
        $this->programCd = $programCd;

        return $this;
    }

    /**
     * Get programCd.
     *
     * @return string|null
     */
    public function getProgramCd()
    {
        return $this->programCd;
    }

    /**
     * Set rhBlood.
     *
     * @param string|null $rhBlood
     *
     * @return PsnPersonMaster
     */
    public function setRhBlood($rhBlood = null)
    {
        $this->rhBlood = $rhBlood;

        return $this;
    }

    /**
     * Get rhBlood.
     *
     * @return string|null
     */
    public function getRhBlood()
    {
        return $this->rhBlood;
    }

    /**
     * Set contactTelephoneNo.
     *
     * @param string|null $contactTelephoneNo
     *
     * @return PsnPersonMaster
     */
    public function setContactTelephoneNo($contactTelephoneNo = null)
    {
        $this->contactTelephoneNo = $contactTelephoneNo;

        return $this;
    }

    /**
     * Get contactTelephoneNo.
     *
     * @return string|null
     */
    public function getContactTelephoneNo()
    {
        return $this->contactTelephoneNo;
    }

    /**
     * Set faxNo.
     *
     * @param string|null $faxNo
     *
     * @return PsnPersonMaster
     */
    public function setFaxNo($faxNo = null)
    {
        $this->faxNo = $faxNo;

        return $this;
    }

    /**
     * Get faxNo.
     *
     * @return string|null
     */
    public function getFaxNo()
    {
        return $this->faxNo;
    }

    /**
     * Set directPhoneNo.
     *
     * @param string|null $directPhoneNo
     *
     * @return PsnPersonMaster
     */
    public function setDirectPhoneNo($directPhoneNo = null)
    {
        $this->directPhoneNo = $directPhoneNo;

        return $this;
    }

    /**
     * Get directPhoneNo.
     *
     * @return string|null
     */
    public function getDirectPhoneNo()
    {
        return $this->directPhoneNo;
    }

    /**
     * Set extPhoneNo.
     *
     * @param string|null $extPhoneNo
     *
     * @return PsnPersonMaster
     */
    public function setExtPhoneNo($extPhoneNo = null)
    {
        $this->extPhoneNo = $extPhoneNo;

        return $this;
    }

    /**
     * Get extPhoneNo.
     *
     * @return string|null
     */
    public function getExtPhoneNo()
    {
        return $this->extPhoneNo;
    }

    /**
     * Set didPhoneNo.
     *
     * @param string|null $didPhoneNo
     *
     * @return PsnPersonMaster
     */
    public function setDidPhoneNo($didPhoneNo = null)
    {
        $this->didPhoneNo = $didPhoneNo;

        return $this;
    }

    /**
     * Get didPhoneNo.
     *
     * @return string|null
     */
    public function getDidPhoneNo()
    {
        return $this->didPhoneNo;
    }

    /**
     * Set extExtPhoneNo.
     *
     * @param string|null $extExtPhoneNo
     *
     * @return PsnPersonMaster
     */
    public function setExtExtPhoneNo($extExtPhoneNo = null)
    {
        $this->extExtPhoneNo = $extExtPhoneNo;

        return $this;
    }

    /**
     * Get extExtPhoneNo.
     *
     * @return string|null
     */
    public function getExtExtPhoneNo()
    {
        return $this->extExtPhoneNo;
    }
}
