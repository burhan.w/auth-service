<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAccount
 *
 * @ORM\Table(name="user_account")
 * @ORM\Entity
 */
class UserAccount
{
    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_pwd", type="string", length=50, nullable=true)
     */
    private $userPwd;



    /**
     * Get userId.
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set userPwd.
     *
     * @param string|null $userPwd
     *
     * @return UserAccount
     */
    public function setUserPwd($userPwd = null)
    {
        $this->userPwd = $userPwd;

        return $this;
    }

    /**
     * Get userPwd.
     *
     * @return string|null
     */
    public function getUserPwd()
    {
        return $this->userPwd;
    }
}
