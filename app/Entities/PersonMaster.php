<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonMaster
 *
 * @ORM\Table(name="person_master")
 * @ORM\Entity
 */
class PersonMaster
{
    /**
     * @var int
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fname_th", type="string", length=50, nullable=true)
     */
    private $fnameTh;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lname_th", type="string", length=50, nullable=true)
     */
    private $lnameTh;



    /**
     * Get personId.
     *
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set fnameTh.
     *
     * @param string|null $fnameTh
     *
     * @return PersonMaster
     */
    public function setFnameTh($fnameTh = null)
    {
        $this->fnameTh = $fnameTh;

        return $this;
    }

    /**
     * Get fnameTh.
     *
     * @return string|null
     */
    public function getFnameTh()
    {
        return $this->fnameTh;
    }

    /**
     * Set lnameTh.
     *
     * @param string|null $lnameTh
     *
     * @return PersonMaster
     */
    public function setLnameTh($lnameTh = null)
    {
        $this->lnameTh = $lnameTh;

        return $this;
    }

    /**
     * Get lnameTh.
     *
     * @return string|null
     */
    public function getLnameTh()
    {
        return $this->lnameTh;
    }
}
