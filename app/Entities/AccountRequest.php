<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountRequest
 *
 * @ORM\Table(name="account_request")
 * @ORM\Entity
 */
class AccountRequest
{
    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="service_type", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $serviceType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_name", type="string", length=200, nullable=true)
     */
    private $serviceName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_url", type="string", length=200, nullable=true)
     */
    private $serviceUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_method", type="string", length=50, nullable=true)
     */
    private $serviceMethod;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_format", type="string", length=100, nullable=true)
     */
    private $serviceFormat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="service_flag", type="string", length=1, nullable=true)
     */
    private $serviceFlag;



    /**
     * Set userId.
     *
     * @param string $userId
     *
     * @return AccountRequest
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set serviceType.
     *
     * @param int $serviceType
     *
     * @return AccountRequest
     */
    public function setServiceType($serviceType)
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    /**
     * Get serviceType.
     *
     * @return int
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * Set serviceName.
     *
     * @param string|null $serviceName
     *
     * @return AccountRequest
     */
    public function setServiceName($serviceName = null)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName.
     *
     * @return string|null
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Set serviceUrl.
     *
     * @param string|null $serviceUrl
     *
     * @return AccountRequest
     */
    public function setServiceUrl($serviceUrl = null)
    {
        $this->serviceUrl = $serviceUrl;

        return $this;
    }

    /**
     * Get serviceUrl.
     *
     * @return string|null
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * Set serviceMethod.
     *
     * @param string|null $serviceMethod
     *
     * @return AccountRequest
     */
    public function setServiceMethod($serviceMethod = null)
    {
        $this->serviceMethod = $serviceMethod;

        return $this;
    }

    /**
     * Get serviceMethod.
     *
     * @return string|null
     */
    public function getServiceMethod()
    {
        return $this->serviceMethod;
    }

    /**
     * Set serviceFormat.
     *
     * @param string|null $serviceFormat
     *
     * @return AccountRequest
     */
    public function setServiceFormat($serviceFormat = null)
    {
        $this->serviceFormat = $serviceFormat;

        return $this;
    }

    /**
     * Get serviceFormat.
     *
     * @return string|null
     */
    public function getServiceFormat()
    {
        return $this->serviceFormat;
    }

    /**
     * Set serviceFlag.
     *
     * @param string|null $serviceFlag
     *
     * @return AccountRequest
     */
    public function setServiceFlag($serviceFlag = null)
    {
        $this->serviceFlag = $serviceFlag;

        return $this;
    }

    /**
     * Get serviceFlag.
     *
     * @return string|null
     */
    public function getServiceFlag()
    {
        return $this->serviceFlag;
    }
}
