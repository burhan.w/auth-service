<?PHP 
    namespace App\Classes;
    
    class Env {
        private $dotEnv;

    	public function __construct($p_base_path) {
            $this->dotEnv = \Dotenv\Dotenv::create($p_base_path);
            $this->dotEnv->load(); 
        }

        public function getEnv($key){
            return $this->dotEnv->load()[$key];
        }

    }
    