<?PHP 
namespace App\Classes;

use App\Repositories\UserAccountRepository;
use App\Repositories\AccountRequestRepository;
use Firebase\JWT\JWT;

trait Authorization{
    private $userId;
    private $availableFormat = [
        'application/json',
        'text/csv',
        'application/xml'
    ];
    private $format;
    private $objectReturn = array();

    private $authenResponse;
    private $authorResponse;

    // private function hasPlatform(){
    //     $headers = apache_request_headers();
    //     $platform = isset($headers['Platform'])?$headers['Platform']:'application';
    //     return $platform;
    // }

    private function hasAuthorize($platform){        // ตรวจสอบ Authentication และ Authorization 
        $bln = '';
        if($platform == 'user'){
            $bln = $this->checkAuthenLdap();
        }else if($platform == 'application'){
            $bln = $this->checkAuthenDb();
        }else{
            return json_encode(['statusCode'=>401]);
        }

        if(!$bln){
            // http_response_code(401);
            // exit;
            return json_encode(['statusCode'=>401]);
        }else{
            $data_account_request = $this->checkAuthorize();
            if($data_account_request['statusCode'] != 200){
                // http_response_code($code['statusCode']);
                // exit;
                return json_encode($data_account_request);
            }
            // $data = array();
            $accessToken = $this->encryptToken($data_account_request);
            return json_encode($accessToken);
            // echo $code['filename'];
        }
    }

    public function getFormat(){
        return $this->format;
    }

    private function checkAuthenLdap(){
        $buasriId = '';
        $buasriPwd = '';

        if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
            $buasriId = $_SERVER['PHP_AUTH_USER'];  //จะไป define ใน sever ทดสอบโดยกรอกใน authorize ของ postman
            $buasriPwd = $_SERVER['PHP_AUTH_PW'];    //จะไป define ใน sever ทดสอบโดยกรอกใน authorize ของ postman    

            // echo "user: ".$buasriId;

            $host ='ldap://ldap.swu.ac.th';
            $rdn = "uid={$buasriId},dc=swu,dc=ac,dc=th";

            $ldapConn = @ldap_connect($host);   // ต้องมี @ ไม่งั้น Warnning เวลากรอกชื่อผู้ใช้ผิด

            if($ldapConn){
                
                $ldapBind = @ldap_bind($ldapConn, $rdn, $buasriPwd);     // ต้องมี @ ไม่งั้น Warnning เวลากรอกชื่อผู้ใช้ผิด
                
                if($ldapBind){
                    $this->userId = $buasriId;
                    return true;
                }else{
                    // echo "ไม่มี Buasri";
                    return false;
                }
            }else{
                // echo "เชื่อมต่อไม่สำเร็จ";
                return false;
            }
        }else{
            header('WWW-Authenticate: Basic realm="Please input Buasri ID:"');
        }
    }
    
    public function checkAuthenDb(){
        $user = '';
        $pass = '';

        if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
            $user = $_SERVER['PHP_AUTH_USER'];  //จะไป define ใน sever ทดสอบโดยกรอกใน authorize ของ postman    // user1:123456
            $pass = $_SERVER['PHP_AUTH_PW'];    //จะไป define ใน sever ทดสอบโดยกรอกใน authorize ของ postman    
        }else{
            return false;
        }

        $userRep = new UserAccountRepository();
        if ($userRep->checkUserPwd($user, $pass) == null) {
            return false;
        }else{
            $this->userId = $user;
            return true;
        }
    }

    private function checkAuthorize(){
        $headers = apache_request_headers();
        $accountRep = new AccountRequestRepository();
        // \var_dump($headers);
        $service_method = $_SERVER['REQUEST_METHOD'];
        // $buasriId = $this->userId;

        $this->format = $_SERVER['HTTP_ACCEPT'];

        if((strlen($this->format) == 0) || (!in_array($this->format, $this->availableFormat))){
            return ['statusCode'=>415];         // 415 Unsupported Media Type
            // http_response_code(415);
            // exit;
        }

        $serviceName = $headers['Service-Name'];
        
        $data = $accountRep->checkRequest($this->userId, $serviceName, $service_method, $this->format);
       
        // echo $url;
        // var_dump ($data);
        // exit;
        if ($data == null) {
            return ['statusCode'=>401];
        }else{
            if($data['serviceFlag'] == 'N'){
                return ['statusCode'=>503];
            }
            $dataFormat = $this->formatServiceDetail($data, $service_method, $this->format);
            return ['serviceDetail'=>$dataFormat, 'statusCode'=>200];
        }
    
    }

    private function encryptToken($data){
        $secretKey = base64_decode(getenv('SECRET_KEY'));
        // $tokenId    = base64_encode(1);
        $rdm = rand(0, 9999);
        $tokenId    = base64_encode($rdm);
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;             //Adding 10 seconds
        $expire     = $notBefore + 180;            // Adding 5 minutes
        $serverName = $_SERVER['SERVER_NAME'];
        $payload = [
            'iat'  => $issuedAt,            // Issued at: time when the token was generated
            'jti'  => $tokenId,             // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,          // Issuer
            'nbf'  => $notBefore,           // Not before
            'exp'  => $expire,              // Expire
            'data' => $data
        ];

        $jwt = JWT::encode(
            $payload,      //Data to be encoded in the JWT
            $secretKey, // The signing key
            'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );
        // echo $jwt;
        return $jwt;
    }

    private function decryptToken($token){
        $secretKey = base64_decode(getenv('SECRET_KEY'));
        
        if (isset($token)) {                        
            if (preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $token, $matches)) {
                $jwt =  $matches[1];                
                try {
                    $out = JWT::decode($jwt, $secretKey, array('HS512'));
                    // print_r($out);
                    return $out;
                } catch (\Firebase\JWT\ExpiredException $expire) {
                    // }catch (\Firebase\JWT\ExpiredException $expire){
                    // echo "Token expired";
                    return false;
                }                                
            }            
        }
    }

    private function formatServiceDetail($data, $method, $format){
        // var_dump($data);

        $data['serviceMethod'] = $method;
        $data['serviceFormat'] = $format;

        return $data;
    }

   

}