<?PHP 
namespace App\Classes;

trait Response
{
    public function response($data, $name="file", $root='', $element=''){
        $format = $this->getFormat();

        if($format == "application/json"){
            return $this->json($data);
        }else if($format == "text/csv"){
            return $this->csv($data, $name);
        }else if($format == "application/xml"){
            return $this->xml($data, $root, $element);
        }
    }

    public function json($data){
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function csv($data, $name='file'){
        header('Content-Type: text/csv');
        header('Conttent-Disposition: attachment; filename:"'.$name.'.csv"');

        if(count($data)>0){
            $fp = fopen('php://output', 'w');
            fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));   // char() แปลงจากรหัสเป็นตัวหนังสือ
            fputcsv($fp, array_keys($data[0]));

            foreach ($data as $row => $cols) {
                fputcsv($fp, $cols);
            }
            fclose($fp);
        }else{
            http_response_code(404);
        }
    }

    public function xml($data,$root='datas',$element='data') {
        header('Content-type: application/xml charset=utf-8');
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><' . $root . '/>');        

        $this->xmlDetail($xml,$data,$element);
        
        print $xml->asXML();
    }
    
    public function xmlDetail(&$xml,$data,$element) {
        foreach ($data as $row => $col) { 
            if (is_array($col)) { 
                $xmlSubNode = $xml->addChild($element);
                $this->xmlDetail($xmlSubNode,$col,$element);
            } else {
                $xml->addChild($row, $col);
            }
        }
    }

}