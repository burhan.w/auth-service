<?PHP 
    namespace App\Classes;
    use AltoRouter;
    class RoutingDispatcher {
        protected $match;
        protected $controller;
        protected $method;
    
        public function __construct(AltoRouter $router)
        { 
            $this->match = $router->match(); 
            if($this->match){ 
                $params = $this->match['params'];
                $target = $this->match['target'];

                if (isset($params['controller'])) {
                    $paramController = $params['controller'];
                    unset($params['controller']);       //ลบ Array ที่ชื่อ  Controller เพื่อยกเลิกการส่ง Paramiter ชื่อ Controller ไปยัง Method ที่ระบุ
                } else {
                    $paramController = "";
                }
                if (isset($params['method'])) {
                    $paramMethod = $params['method'];
                    unset($params['method']);           //ลบ Array ที่ชื่อ Method เพื่อยกเลิกการส่ง Paramiter ชื่อ Method ไปยัง Method ที่ระบุ
                    
                } else {
                    $paramMethod = "";
                }
            
                $target = str_replace("{controller}",$paramController,$target); 
                $target = str_replace("{method}",$paramMethod,$target); 

                list($controller, $method) = explode('@', $target); 
                        
                $this->controller = $controller;
                $this->method = $method; 
                
                if(is_callable(array($this->controller, $this->method))){     //is_callable ใช้ตรวจสอบ class และ method ในระบบนี้มีหรือไม่
                    call_user_func_array(array(new $this->controller, $this->method), array($params));  //call_user_func_array(array(new class, method), array(paramiter)) ทำการเรียก method ใน class ที่ระบุ โดยส่ง paramiter 
                }else{ 
                    echo "The method {$this->method} is not defined in {$this->controller}";
                } 
            }else{
                http_response_code(404);			
            }
        }
    }
    
    