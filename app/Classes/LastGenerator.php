<?PHP
    namespace App\Classes;

    use Doctrine\ORM\Id\AbstractIdGenerator;
    // use App\Entities\Lastnumber;
    use App\Entities\SysGenerate;

    class LastGenerator extends AbstractIdGenerator {
        public function generate(\Doctrine\ORM\EntityManager $em, $entity) {
            $length = $entity->getLength();   //
            $prefix = $entity->getPrefix();
            $className = get_class($entity);
            $tableName =  $em->getClassMetadata($className)->getTableName();
            
            $allRecord = $em->getRepository(SysGenerate::class)->findOneBy([
                // 'tableCd'=>$className,
                'tableCd'=>$tableName,
                'keyCd'=>$prefix
            ]);  

            $LastNo=1;
            if($allRecord != null){
                // update
                $LastNo = $allRecord->getLastNo()+1;
                $allRecord->setLastNo($LastNo);
                $em->flush();
            }else{
                // insert
                $lastNumber = new SysGenerate();
                $lastNumber->setTableCd($tableName);
                // $lastNumber->setPrefix($prefix);
                $lastNumber->setKeyCd($prefix);
                $lastNumber->setLastNo($LastNo);
                // var_dump($lastNumber);
                $em->persist($lastNumber);
                $em->flush();
            }

            if(strlen($prefix)>0){
                $str = $prefix.str_pad($LastNo, $length,'0', STR_PAD_LEFT);
                return $str;
            }else{
                return $LastNo;
            }  
        }
    }
