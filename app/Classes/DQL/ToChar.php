<?php
    namespace App\Classes\DQL;
    use Doctrine\ORM\Query\AST\Functions\FunctionNode;
    use Doctrine\ORM\Query\Lexer;
    use Doctrine\ORM\Query\SqlWalker;

    class ToChar extends FunctionNode
    {
        private $arithmeticExpression;
        private $format;

        public function getSql(SqlWalker $sqlWalker)
        {
            return 'TO_CHAR(' . 
                $sqlWalker->walkSimpleArithmeticExpression($this->arithmeticExpression). 
                ','.
                $sqlWalker->walkSimpleArithmeticExpression($this->format).
            ')';
        }
        public function parse(\Doctrine\ORM\Query\Parser $parser)
        {
            $lexer = $parser->getLexer();
            var_dump($lexer);
            $parser->match(Lexer::T_IDENTIFIER);
            $parser->match(Lexer::T_OPEN_PARENTHESIS);
            $this->arithmeticExpression = $parser->SimpleArithmeticExpression();

            $parser->match(Lexer::T_COMMA);
            $this->format = $parser->SimpleArithmeticExpression();

            $parser->match(Lexer::T_CLOSE_PARENTHESIS);
        }
    }
