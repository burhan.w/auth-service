<?php
    namespace App\Classes\DQL;
    use Doctrine\ORM\Query\AST\Functions\FunctionNode;
    use Doctrine\ORM\Query\Lexer;
    use Doctrine\ORM\Query\SqlWalker;

    class ConcatHello extends FunctionNode
    {
        private $arithmeticExpression;
        public function getSql(SqlWalker $sqlWalker)
        {
            // CONCATHELLO ต้องเป็นชื่อฟังก์ชันจริงใน Database
            return 'CONCATHELLO(' . $sqlWalker->walkSimpleArithmeticExpression(
            $this->arithmeticExpression
            ) . ')';
        }
        public function parse(\Doctrine\ORM\Query\Parser $parser)
        {
            $lexer = $parser->getLexer();
            // var_dump($lexer);
            $parser->match(Lexer::T_IDENTIFIER);
            $parser->match(Lexer::T_OPEN_PARENTHESIS);
            $this->arithmeticExpression = $parser->SimpleArithmeticExpression();
            $parser->match(Lexer::T_CLOSE_PARENTHESIS);
        }
    }
