<?PHP
    require_once('vendor/autoload.php');

    use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;
    use Doctrine\ORM\Tools\Console\ConsoleRunner;   // Console Runner จะบังคับให้มีไฟล์นี้ เป็นตัวตั้งต้นในการ connect Database

    $isDevMode = false;
    $dbParam = array (
        // 'url'=> 'oci8://sealsdev:seals@localhost/orcl?charset=utf8'  // connect database oracle แบบ url 
        'url'=> 'sqlsrv://seals:seals@localhost\SQLEXPRESS/seals?charset=utf-8'
    );    

    $paths = array(__DIR__ . "/app/Entities");   // ตอน generate entities จะเรียกตาม path ที่กำหนด
    // echo __DIR_;
    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
    $entityManager = EntityManager::create($dbParam, $config);
    
    return ConsoleRunner::createHelperSet($entityManager);