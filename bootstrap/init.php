<?PHP // initial all libraries
    /**
    * Session Start
    */
    if(!isset($_SESSION)) session_start();

    /**
    * Global Constant
    */
    define('BASE_PATH', realpath(__DIR__.'/../'));
    // define('WWW_PATH', realpath(__DIR__.'/../') . "/wwwroot/");

     // set time zone
     date_default_timezone_set("Asia/Bangkok");

    /**
    * Autoload
    */
    $loader = require_once BASE_PATH . '/vendor/autoload.php';

    /**
    * Load Environment
    */
    
    $dotENV = new \App\Classes\Env(BASE_PATH);
    // echo ($dotENV->getEnv('FTP_URL'));
    /**
    * Initial Database
    */
    // $entityManager = new \App\Classes\Database(BASE_PATH);

    /**
    * Initial Routing
    */
    $router = new AltoRouter();
    $router->setBasePath($dotENV->getEnv('APP_PATH'));
    require_once BASE_PATH.'/app/routing/api.php';

    // echo BASE_PATH;
    // echo "<br>".getenv('APP_PATH')."<br>";
    // echo getenv('APP_URL');
    // var_dump($router->match());
    // exit;
    // require_once BASE_PATH.'/app/routing/api.php';
    new \App\Classes\RoutingDispatcher($router);